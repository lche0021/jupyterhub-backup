import pandas as pd
import numpy as np
import sys
sys.path.append('/mnt/code')
from constants import PROCESSED_DATASET
import seaborn as sns
import matplotlib 
matplotlib.rcParams['figure.dpi'] = 300
import matplotlib.pyplot as plt
from load_dataset import load_dataset
from preprocess.read_raster import get_raster_colnames, get_raster_names
from preprocess.absence_generation import get_dataset_by_species
from preprocess.absence_generation import get_coordinates_of_species

def open_datasets(taxon_id):
    """
    A function which takes a taxon id and opens the relevant files as well as begins the processing of the datasets
    input: taxon id
    returns: two pandas dataframes, one containing the presence information, one containing the absence information
    """
    from preprocess.absence_generation import get_dataset_by_species
    from preprocess.absence_generation import get_coordinates_of_species

    ## import presence dataset # lat long
    species_presence = get_dataset_by_species(taxon_id)
    species_presence.reset_index(drop=True, inplace=True)

    ## import absence dataset # Long Lat
    from constants import PROCESSED_DATASET
    species_absence = pd.read_csv(PROCESSED_DATASET / 'absence_{}.csv'.format(taxon_id))
    failed_absence = pd.read_csv(PROCESSED_DATASET / 'failed_absence_{}.csv'.format(taxon_id)) 
    
    ## get the vicgrid coordinates
    presence_coords_in_vicgrid = pd.DataFrame(get_coordinates_of_species(taxon_id), columns = ['longitudedd_num','latitudedd_num'])
    
    ## swap to make species presence follow long then lat setup the same as the absence data
    ## switch to vicgrid coordinates as well

    species_presence['latitudedd_num'] = presence_coords_in_vicgrid['latitudedd_num']
    species_presence['longitudedd_num'] = presence_coords_in_vicgrid['longitudedd_num']
    
    return species_presence, species_absence

def make_sizes(species_presence, species_absence):
    """
    Removes the unwanted columns from the presence dataframe
    Populates the absence dataframe with the required columns for it to match the presence dataframe
    input: presence and absence dataframes
    output: resized presence and absence dataframes
    """
    ### We will now seek to make the two dataframes into the same size so that we can compare them in modelling.
    ## cleaning the presence data -- removing unwanted columne
    absence_df = pd.DataFrame(columns=species_presence.columns) # create empty absence dataframe to populate with values

    # delete useless columns from both dataframe
    cols_to_drop = ['taxon_id', 'ufi', 'rating_int', 'survey_start_date', 'lat_long_accuracydd_int', 'park_id','survey_id','creation_tsp', 'reliability_txt']
    species_presence = species_presence.drop(cols_to_drop, 1)
    absence_df = absence_df.drop(cols_to_drop, 1)
    
    ## We are populating the absence_df with the values from species_absence. We still have NaN's where we have not populated it with anything yet
    absence_df[species_absence.columns] = species_absence[species_absence.columns]
    
    return species_presence, absence_df

def encode_catagorical(dataframe, col_name):
    """
    Takes a dataframe and a column name, makes that column catagorical with integers
    """
    # let us first remove all NaN values
    dataframe = dataframe.fillna('0')
    
    from sklearn.preprocessing import OrdinalEncoder
    enc = OrdinalEncoder()
    X = list(dataframe[str(col_name)].apply(lambda x: [x]))
    enc.fit(X)
    enc.categories_
#     display(enc.categories_)
    dataframe[str(col_name)] = dataframe[str(col_name)].apply(lambda x: enc.transform([[x]])[0][0])
    return dataframe

def drop_null_cols(dataframe, absence_dataframe):
    """
    Removes any columns which have null in their entirety
    Returns the updated dataframes and a list of columns which have been dropped
    """
    cols = list(dataframe.columns)
    dropped_columns = []
    for column in cols:
        if dataframe[str(column)].isnull().sum() == len(dataframe):
            dataframe.drop(str(column), axis=1, inplace=True)
            absence_dataframe.drop(str(column), axis=1, inplace=True)
            dropped_columns.append(column)
    return dataframe, absence_dataframe, dropped_columns

def drop_rows_with_na(dataframe, column):
    """
    A function which takes a dataframe and drops the rows which have null in a given column
    param: a given dataframe
    param: the column which which to map na values before dropping
    """
    dataframe.dropna(subset=[column], inplace=True)
    return dataframe

def generate_column_distribution_alt(dataframe, column):
    """
    Generates a basic discrete probabilty distribution by counting the discrete values within the column of a dataframe
    param: a dataframe
    param: a column from which to generate a distribution from
    """
    distribution = dataframe[str(column)].value_counts()
    rows = list(distribution.index)
    prob = distribution.apply(lambda x : x/len(dataframe))
    return rows, prob


def get_nan_rows(dataframe, column):
    """
    Returns a dataset containing all of the rows for which the reliability column contains a NaN
    """
    nan_sampling_method = dataframe[dataframe['reliability'].isna()][[column]]
    nan_sampling_method.fillna('Other', inplace=True)
    return nan_sampling_method

def generate_absence_columns(absence_dataframe, presence_dataframe):
    """
    The main function behind generating the absence dataframe so it can be succesfully merged with the presence dataframe
    NOTE: A lot of the work done in this does not eventually get used with it being found that sampling method and record
    count being too difficult to use. Leaving code here in case of future changes, however these columns are later dropped.
    """
    cols = list(presence_dataframe.columns)
    for column in cols:
        if column == 'reliability':
            # Sets the reliability column to unacceptable
            absence_dataframe[column] = absence_dataframe[column].apply(lambda x: 'Unacceptable')
            
        elif column == 'sampling_method_desc':
            # Populating the absence dataframe with sampling methods based on the probabilities of the presence dataframe
            nan_df = get_nan_rows(presence_dataframe, column)
            presence_dataframe[column].fillna('Other', inplace=True)
            rows, inv_prob = generate_column_distribution_alt(presence_dataframe,column)
            absence_dataframe[column] = absence_dataframe[column].apply(lambda x: np.random.choice(rows, 1, p=inv_prob)[0])
        
        elif column == 'record_type':
            # Populating the absence dataframe with record types based on the probabilities of the presence dataframe
            nan_df = get_nan_rows(presence_dataframe, column)
            presence_dataframe[column].fillna('Other', inplace=True)
            rows, inv_prob = generate_column_distribution_alt(presence_dataframe,column)
            absence_dataframe[column] = absence_dataframe[column].apply(lambda x: np.random.choice(rows, 1, p=inv_prob)[0])
            
        elif column == 'sv_record_count':   # is not currently being used in final model, unsure what this column represents
            absence_dataframe[column] = absence_dataframe[column].apply(lambda x: np.random.choice([1,2,3,4,5], 1, p=[0.4, 0.2, 0.2, 0.1, 0.1])[0]) # will pick a number 1-5 biased towards 1
        
        elif column == 'total_count_int':
            presence_dataframe[column].fillna(0, inplace=True)
            rows, inv_prob = generate_column_distribution_alt(presence_dataframe,column)
            absence_dataframe[column] = absence_dataframe[column].apply(lambda x: np.random.choice(rows, 1, p=inv_prob)[0])

    return absence_dataframe, presence_dataframe

def generate_presence_column(presence_dataframe):
    """
    Modifies the reliability column of the presence dataframe to set everything to acceptable
    The reasoning behind this is that we assume that all of the points given to us are acceptable. None are explicitly absence
    """
    cols = list(presence_dataframe.columns)
    for column in cols:
        if column == 'reliability':
            presence_dataframe[column] = presence_dataframe[column].apply(lambda x: 'Unacceptable' if x == "Unconfirmed" else x)
            presence_dataframe[column] = presence_dataframe[column].notna().apply(lambda x: 'Acceptable') # should not be 
    return presence_dataframe

def merge_dataset(species_presence, absence_df):
    ## We can now merge the two dataframe's together
    merged_dataset = pd.concat([species_presence, absence_df])

    ## randomise the dataframe
    merged_dataset = merged_dataset.sample(frac=1)

    if 'total_count_int' in merged_dataset.columns:
        merged_dataset.drop(['total_count_int'], 1, inplace=True) 
    ## drop any rows with na values
    merged_dataset = drop_rows_with_na(merged_dataset, 'reliability')
    
    ## binarise the reliability column into numbers instead of strings
    merged_dataset = encode_catagorical(merged_dataset, 'reliability')
    
    ######## INSTEAD OF THE ABOVE get_dummmies() WE WILL DROP THESE COLUMNS AS THEY HAVE NO IMPACT AND INTERFERE WITH MODEL DIM
    merged_dataset.drop(['sampling_method_desc', 'record_type'], 1, inplace=True) 
    print(merged_dataset.columns)

    ## drop any columns we do not need, in this case, sv_record_count has unclear purpose, so will not use
    merged_dataset.drop(['sv_record_count'], 1, inplace=True) 
    
    return merged_dataset


def make_dataset(taxon_id):
    """
    Primary function which builds the dataset
    input: a taxon_id
    output: the final_dataset for that species
    """
    # open dataset
    species_presence, species_absence = open_datasets(taxon_id)

    # make the datasets the correct shapes
    species_presence, absence_df = make_sizes(species_presence, species_absence)

    ## Remove cols with nulls in the column, can happen for specific species
    species_presence,absence_df, columns = drop_null_cols(species_presence, absence_df)

    ## Generate the absence columns according the information in the presence dataset
    absence_df, species_presence = generate_absence_columns(absence_df, species_presence)

    ## Change the species dataframe reliability column acceptable if labelled
    species_presence = generate_presence_column(species_presence)
    
    ## merge the presence and absence datasets
    final_dataset = merge_dataset(species_presence, absence_df) 
    return final_dataset

def make_split(dataframe):
    """
    A function which splits the dataframe into a test and train set
    param: a dataframe, assumes dataframe has a reliability column
    return: the train/test features and labels
    """
    import numpy as np
    labels = np.array(dataframe['reliability'])
    features= dataframe.drop('reliability', axis = 1)
    feature_list = list(features.columns)
    features = np.array(features)

    from sklearn.model_selection import train_test_split
    train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size = 0.25, random_state = 42)
    ## We will be testing on 25% of dataset
    return train_features, test_features, train_labels, test_labels
    
    
def random_forest_model(train_features, train_labels):
    """
    A random forest clasifier from scikit learn
    param: The training features and labels
    return: the model
    """
    from sklearn.ensemble import RandomForestClassifier
    rf = RandomForestClassifier(n_estimators = 100, random_state = 42, max_depth = 5)
    rf.fit(train_features, train_labels);
    
    return rf

def make_predictions(model, test_features, test_labels):
    """
    A function which takes a model, test features and test labels and prints the prediction scores of the model
    param: The training features and labels
    return: the predictions
    """
    predictions = model.predict(test_features)

    from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

    print(confusion_matrix(test_labels,predictions))
    print(classification_report(test_labels,predictions))
    print(accuracy_score(test_labels, predictions))
    return predictions

def save_model(model, model_name, taxon_id):
    """
    A function which takes the model and saves it to location specified in constants
    param: model
    param:the name of the model
    param: the taxon_id of the species the model is for
    return: nothing
    output: saves model as a pickle
    """
    import pickle
    from constants import MODELS
    filename = MODELS /'{}_{}.sav'.format(taxon_id, model_name)
    print("Saving model as " + str(filename))
    pickle.dump(model, open(filename, 'wb'))

    return



def load_model(model_name, taxon_id):
    """
    A function which takes a model name and a taxon_id and reads the model from a pickle file
    param: model name
    param: the taxon id of a species
    """
    import pickle
    from constants import MODELS
    filename = MODELS /'{}_{}.sav'.format(taxon_id, model_name)
    model = pickle.load(open(filename, 'rb'))
    return model

def load_scaler(model_name, taxon_id):
    """
    Some models require that you scale them with the same scaler as how the model was trained, this is the method to get same the scalar
    param: a model name
    param: a taxon id of a given species
    return: a scaling model
    """
    import pickle
    from constants import MODELS
    filename = MODELS /'{}_{}_scaler.sav'.format(taxon_id, model_name)
    model = pickle.load(open(filename, 'rb'))
    return model

def plot_auc_roc(model,test_features,test_labels):
    """
    A function which will print a AUC-ROC curve of a model 
    param: a model to test
    param: the test features
    param: the test labels
    """
    from sklearn.metrics import classification_report
    from sklearn.metrics import roc_curve, auc
    import matplotlib.pyplot as plt

    # overall accuracy
    acc = model.score(test_features,test_labels)

    # get roc/auc info
    Y_score = model.predict_proba(test_features)[:,1]
    fpr = dict()
    tpr = dict()
    fpr, tpr, _ = roc_curve(test_labels, Y_score)

    roc_auc = dict()
    roc_auc = auc(fpr, tpr)

    # make the plot
    plt.figure(figsize=(10,10))
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([-0.05, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.grid(True)
    plt.plot(fpr, tpr, label='AUC = {0}'.format(roc_auc))        
    plt.legend(loc="lower right", shadow=True, fancybox =True) 
    plt.show()
    
def print_feature_importances(model):
    """
    When given a random forest will print the importance of the features that are greater than 0
    param: a model, the model must be a random forest for this method to work
    return: nothing
    """
    importances = list(rf.feature_importances_)
    feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
    feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
    feature_importances = filter(lambda x: x[1] > 0.0, feature_importances) 
    [print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];
    return


def svc_model(train_features, train_labels, test_features, taxon_id):
    """
    A function which generates a support vector classifier model
    Will scale the features accordingly and save the scaler used.
    param: The training features and labels, test features and taxon_id
    return: the model as well as the scaled test features
    output: write the scaler to memory
    """
    from sklearn.svm import SVC
    from sklearn.preprocessing import StandardScaler

    clf = SVC(gamma='auto') # most parameters in model left as automatic
    transformer = StandardScaler().fit(train_features)
    transformer.transform(train_features)
    transformer.transform(test_features)

    clf.fit(train_features, train_labels)
    
    # save the transformer
    import pickle
    from constants import MODELS
    filename = MODELS /'{}_svc_scaler.sav'.format(taxon_id)
    pickle.dump(transformer, open(filename, 'wb'))
    
    return clf, test_features

def ada_model(train_features, train_labels):
    """
    A function which will generate an adaptive boost classifier
    param: train features and labels
    return: a model
    """
    from sklearn.ensemble import AdaBoostClassifier
    clf = AdaBoostClassifier(n_estimators=25, random_state=0)
    clf.fit(train_features, train_labels)
    return clf

def gbc_model(train_features, train_labels):
    """
    A function which will generate an gradient boosting classifier
    param: train features and labels
    return: a model
    """
    from sklearn.ensemble import GradientBoostingClassifier
    clf = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0)
    clf.fit(train_features, train_labels)
    return clf

def mlp_model_adam(train_features, train_labels, test_features, taxon_id):
    """
    A function which generates a multi-layer perceptron model
    Will scale the features accordingly and save the scaler used.
    param: The training features and labels, test features and taxon_id
    return: the model as well as the scaled test features
    output: write the scaler to memory
    """
    from sklearn.neural_network import MLPClassifier    
    from sklearn.preprocessing import StandardScaler

    mlp = MLPClassifier(hidden_layer_sizes=(10, 10, 10), max_iter=1000)
    transformer = StandardScaler().fit(train_features)
    transformer.transform(train_features)
    transformer.transform(test_features)

    mlp.fit(train_features, train_labels)
    
    # save the transformer
    import pickle
    from constants import MODELS
    filename = MODELS /'{}_mlp_scaler.sav'.format(taxon_id)
    pickle.dump(transformer, open(filename, 'wb'))
    
    return mlp, test_features

def kneigh_model(train_features, train_labels):
    """
    A function which will generate a k nearest kenighbours model
    param: train features and labels
    return: a model
    """
    from sklearn.neighbors import KNeighborsClassifier
    neigh = KNeighborsClassifier(n_neighbors=10)
    neigh.fit(train_features, train_labels)
    return neigh

def tree_model(train_features, train_labels):
    """
    A function which will generate a decision tree model
    param: train features and labels
    return: a model
    """
    from sklearn import tree
    clf = tree.DecisionTreeClassifier(random_state=0)
    clf.fit(train_features, train_labels)
    return clf


    
def generate_models(dataset, taxon_id):
    """
    A function which takes a dataset and a taxon id and generates all of the above models, saving them all to memory
    Will also print the accuracy of each of them
    param: a dataset to train and test from
    param: the id of a given species
    output: will save the models for each in a location specified in constants
    """
    train_features, test_features, train_labels, test_labels = make_split(dataset)
    
    # Random Forest
    rf = random_forest_model(train_features, train_labels)
    predictions = make_predictions(rf, test_features, test_labels )
    save_model(rf, "random_forest_model", taxon_id)
    
    # Support Vector Classifier
    svc, svc_test_features = svc_model(train_features, train_labels, test_features, taxon_id)
    predictions = make_predictions(svc, svc_test_features, test_labels )
    save_model(svc, "support_vector_classifier", taxon_id)

    # Ada boost
    ada = ada_model(train_features, train_labels)
    predictions = make_predictions(ada, test_features, test_labels )
    save_model(ada, "ada_boost", taxon_id)
    
    # Gradient Boosting Classifier
    gbc = gbc_model(train_features, train_labels)
    predictions = make_predictions(gbc, test_features, test_labels )
    save_model(gbc, "gb_classifier", taxon_id)
    
    ## MLP Adam
    mlp, mlp_test_features = mlp_model_adam(train_features, train_labels, test_features, taxon_id)
    predictions = make_predictions(mlp, mlp_test_features, test_labels )
    save_model(mlp, "MLP_classifier", taxon_id)
    
    ## k-nearest neighbours classifier
    kneigh = kneigh_model(train_features, train_labels)
    predictions = make_predictions(kneigh, test_features, test_labels )
    save_model(kneigh, "kneigh_classifier", taxon_id)
    
    ## decision tree classifier
    tree = tree_model(train_features, train_labels)
    predictions = make_predictions(tree, test_features, test_labels )
    save_model(tree, "tree_classifier", taxon_id)
    
def generate_all_models():
    """
    A simple function which will loop through all of the taxon ids and generate all the models for each
    """
    from constants import TAXON_IDS
    taxon_ids = TAXON_IDS
    for taxon in taxon_ids:
        final_dataset = make_dataset(taxon)
        generate_models(final_dataset, taxon)
        

if __name__ == "__main__": 
    generate_all_models()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    