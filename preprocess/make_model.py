from absence_generation import generate_all
from model_generator import generate_all_models


if __name__ == "__main__": 
    """
    This file has the singular job of generating all of the absence data as well as all the models, will be called by technical user
    """
    ## Will generate the absence data
    generate_all()
    
    ## Will generate the models
    generate_all_models()
    