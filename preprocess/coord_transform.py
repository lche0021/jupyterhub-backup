from osgeo import osr
import numpy as np
# EPSG:4326 is same with WGS84, which is the most standard CRS, it is used inside xls obseration sheet, while EPSG:3111 is australia standard, which is used in raster dataset and shapefile(VMLITE) dataset.

def define_transform(sourceEPSG=4326, targetEPSG=3111):
    """
    define the transform the coordinate reference system
    
    Keyword Arguments:
        sourceEPSG {int} -- source CRS in epsg (default: {4326})
        targetEPSG {int} -- destination CRS in epsg (default: {3111})
    
    Returns:
        osr.CoordinateTransformation -- the transformation carrier
    """
    src = osr.SpatialReference()
    tgt = osr.SpatialReference()
    src.ImportFromEPSG(sourceEPSG)
    tgt.ImportFromEPSG(targetEPSG)
    transform = osr.CoordinateTransformation(src, tgt)
    return transform

def transform_coord(coord):
    """
    transform the coordinate reference system
    
    Arguments:
        coord {[WGS84]} -- [description]
    
    Returns:
        [GDA94] -- [description]
    """
    transform = define_transform()
    transformed_coord = transform.TransformPoints(list(coord.itertuples(name=None, index=False)))
    return np.array(transformed_coord)[:, :-1]


