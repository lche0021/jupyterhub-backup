from osgeo import ogr
import pandas as pd
# from load_dataset import load_dataset
from preprocess.coord_transform import transform_coord


def pdf_to_coord(df):
    coord = df[['longitudedd_num','latitudedd_num']]
    return coord
def flatten_list(lst):
    out = []
    for i in lst:
        for j in i:
            out.append(j)
    return out

def generate_point(coord):
    """
    Takes a single coordinate pair and returns a point object
    :param: a coordinate in the VicGrid94 format
    :return: a dictionary containing the useful shapefiles
    """
    X = coord[0]
    Y = coord[1]
    newPoint = ogr.Geometry(ogr.wkbPoint)   
    newPoint.AddPoint_2D(X,Y)
    return newPoint

def lower_colname(pd):
    """
    Since every columnis written in block letters, the function converts it
    to lower case by using the built-in function .lower().
    :return A csv file with modified column names.
    """
    return pd.rename(columns=dict(pd.columns.map(lambda x : (x, x.lower()))))
    
