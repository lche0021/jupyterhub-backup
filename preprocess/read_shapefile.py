import os
import pandas as pd
from constants import SHAPEFILE_DIR, SHAPEFILE_COL, PROCESSED_SHAPE_DATASET
from osgeo import ogr


def get_processed_shapefile_dataset(dataset_dir=PROCESSED_SHAPE_DATASET):
    all_files = [pd.read_csv(i) for i in dataset_dir.glob('*.csv')]
    dataset_df = pd.concat(all_files, axis='columns')
    return dataset_df

def get_shapefile_dataset(shapefile_dir = SHAPEFILE_DIR, shapefile_col = SHAPEFILE_COL):
    """
    A function to open the shapefiles and to generate a dictionary to store them
    Function relies on the existance of SHAPEFILE_DIR and USEFUL_SHAPEFILE
    :return: a dictionary containing the useful shapefiles
    """
    driver = ogr.GetDriverByName('ESRI Shapefile')    # driver required to open shapefiles
    
    shapefile_dirs = list(shapefile_dir.glob('*'))
    dataset_dict = {}
    for p in shapefile_dirs:
        if p.name in shapefile_col:
            dataset_dict.update({p.name : p / (p.name)})
    return dataset_dict


if __name__ == '__main__':
    dataset = get_shapefile_dataset()
    print(dataset["VMLITE_FOREST_SU2"])
