import glob
import pandas as pd
from spectral import open_image
from constants import RASTER_DIR, PROCESSED_RASTER_DATASET, RASTER_COL
from preprocess.helper import flatten_list


def get_processed_raster_dataset(raster_dataset_dir=PROCESSED_RASTER_DATASET):
    """
    This function is processing the raster dataset.
    :return
    """
    dataset_list = [pd.read_csv(i) for i in raster_dataset_dir.glob('*.csv')]
    dataset_df = pd.concat(dataset_list, axis='columns')
    return dataset_df

def get_raster_names():
    """
    :return
    """
    return [i.stem for i in PROCESSED_RASTER_DATASET.glob('*.csv')]

def get_raster_dataset(raster_dir=RASTER_DIR):
    """
    Obtain the raster file and uses spectral library, open_image to store the file
    in a dictionary called dataset_dict.
    :return: A dictionary labeled as dataset_dict.
    """
    raster_dirs = list(raster_dir.glob('*'))
    dataset_dict = {}
    for p in raster_dirs:
        if p.name in RASTER_COL: 
            dataset_dict.update({p.name : open_image(p / ((p.name)+'.hdr'))})
    return dataset_dict

def get_raster_colnames():
    """
    Retrieve raster column names in the processed raster dataset list.
    :return:  
    """
    return list(get_processed_raster_dataset().columns)
    return flatten_list([pd.read_csv(i).columns for i in PROCESSED_RASTER_DATASET.glob('*.csv')])
