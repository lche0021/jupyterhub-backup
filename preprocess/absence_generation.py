import sys
sys.path.append("/mnt/code/")

import os
import math
import pandas as pd
import random
import numpy as np

from osgeo import ogr
from preprocess.coord_transform import transform_coord
from preprocess.helper import pdf_to_coord
from preprocess.helper import generate_point 
from preprocess.get_species_obs import get_species_obs
from pathlib import Path
from constants import OBS_CSV, OBS_COL
from preprocess.helper import lower_colname
from load_dataset import load_dataset
from preprocess.environmental_factors_from_point import environmental_factors_from_point
from constants import PROCESSED_DATASET



# Machine Learning
from sklearn import svm
from sklearn.neighbors import LocalOutlierFactor
from sklearn.ensemble import IsolationForest
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib 


def get_dataset_by_species(taxon_id):
	"""
	:param: the taxon_id of a species within the dataset
	:return: the dataset of the given species containing only the observations of that species
	"""
	dataset = load_dataset()
	dataset = dataset[dataset.taxon_id == int(taxon_id)]
	
	return dataset

def get_dataset_count(taxon_id):
	"""
	 A simple function which returns the number of observations of a species
	:param: the taxon_id of a species within the dataset
	:return: the number of observations of the given species
	"""   
	dataset = load_dataset()
	dataset = dataset[dataset.taxon_id == int(taxon_id)]
	
	return len(dataset)
  
def get_coordinates_of_species(taxon_id):
	"""
	Takes the taxon_id of a species and returns the coordinates of that species
	:param: the taxon_id of a species within the dataset
	:return: coordinates in vicgrid formatting
	"""
	dataset = get_dataset_by_species(taxon_id) # get the species
	
	coordinate_cols = dataset[['latitudedd_num','longitudedd_num']] # transform to just coords

	columnOrder = ['longitudedd_num', 'latitudedd_num']  # switches the order of the cols
	coordinate_cols = coordinate_cols.reindex(columns=columnOrder)

	# get the transformed coordinates
	transformed_coord_lst = transform_coord(coordinate_cols)
	
	return transformed_coord_lst


def training_testing(taxon_id):
	"""
	Takes a taxon id of a species, and returns a training/testing set of the environmental factors of that species.
	Split 1/9 test, 8/9 training. Reasoning is that we want most of the observations in the model for absence data    
	:param: the taxon_id of a species within the dataset
	:return: a training and testing set
	"""    
	species_data = get_dataset_by_species(taxon_id)
	species_data.reset_index(drop=True, inplace=True)
	
	
	coordinates = get_coordinates_of_species(taxon_id)
	coordinates = pd.DataFrame(coordinates, columns = ['longitudedd_num','latitudedd_num']) # make into pd dataframe

	# the below line will join the lat/long columns with the environmental factors
	# stripping away the rest
	environmental_features = coordinates.join(species_data.iloc[:,16:])
	
	
	# split into training/testing data
	split_proportion = 9
	index_list = []
	for i in range(len(environmental_features)):
		index_list.append(i)
	
	test_indices = random.sample(index_list, len(environmental_features)//split_proportion)
	train_indices = list(set(index_list) - set(test_indices))
	
	env_test = environmental_features.iloc[test_indices,]
	env_train = environmental_features.iloc[train_indices,]
	
	return env_train,env_test

def one_class_svm(training_data, testing_data):
	"""
	Generates a model based on a one class svm
	:return: the SVM model
	"""
	clf = svm.OneClassSVM(nu=0.1,kernel="rbf",gamma=0.1)
	clf.fit(env_train)
	
	return clf

def local_outlier_factor(training_data, testing_data):
	"""
	A function which takes training and testing data and returns an LOF model
	:param: training data
	:param: testing data
	:returns: the LOF model
	"""
	clf = LocalOutlierFactor(n_neighbors = 34,novelty=True)
	clf.fit(training_data)
	
	return clf

def isolation_forest(training_data, testing_data):
	"""
	Contamination: The amount of contamination of the data set, 
			   i.e. the proportion of outliers in the data set.
	:param: training data
	:param: testing data
	:returns: the isolation forest model
	"""
	rng = np.random.RandomState(42)
	iso_forest = IsolationForest(behaviour='new', max_samples=100,
						  random_state=rng, contamination=0.1)
	iso_forest.fit(env_train)
	
	return iso_forest	

# UNFINISHED FUNCTION FOR CIRCLES METHOD: DECIDED NOT TO PERSUE THIS POSSABILITY IN THE END
# def buffer_region(transformed_coord_list, buffer_distance):
#	 """
#	 :param: a transformed list of coordinates
#	 :return: a geometry of the buffer zone around all points
#	 """
#	 list_of_buffers = []
#	 for coord in transformed_coord_lst:
#		 newPoint = generate_point(coord)   # generates a point object with geometry
#		 buffer_distance = 50000

#		 polygon = newPoint.Buffer(buffer_distance)

#		 list_of_buffers.append(polygon)

#	 # loop through the shapes within the geometry, taking the union until all 
#	 # are within the object.
#	 overall_geometry = list_of_buffers[0]
#	 for geometry in list_of_buffers:
#		 overall_geometry = overall_geometry.Union(geometry)
		
#	 return overall_geometry



def generate_absence_points(number_of_points, taxon_id):
	"""
	A function which generates the absence data for a given species 
	:param: the number of absence points we want generated
	:param: taxon id of species we are generating data for
	:returns: the LOF model
	"""    
	absence_within_buffer = 0 # number of absence points allowed within the buffer
	
	# ML model
	env_train,env_test = training_testing(taxon_id)
	clf = local_outlier_factor(env_train, env_test)
	
	# stores the region overwhich we will be generating absence points
	long_min = 1970350
	long_max = 2991050
	lat_min = 2258025
	lat_max = 2948575
	
	counter = 0
	column_list = env_train.columns.tolist()
	absence_points = pd.DataFrame(columns = column_list) # will store the the absence points, creates empty pandas df with column headings
	failed_absence_points = pd.DataFrame(columns = column_list) # will store the the absence points, creates empty pandas df with column headings

	random.seed(1) # sets a random seed for reproducability
	
	while counter < number_of_points:
		# generate random points
		random_lat = random.uniform(lat_min, lat_max)
		random_long = random.uniform(long_min, long_max)

		# generate environmental variables for point  
		coordinates = pd.DataFrame([[random_long, random_lat]],columns = ['longitudedd_num','latitudedd_num'])
		
		# merge the environmental values and the coordinates
		env_values = environmental_factors_from_point(random_lat, random_long)
		feature_row = coordinates.join(env_values)
		# at this point 
		
		# now to reorder the columns in our environmental features
		training_column_list = env_train.columns.tolist()
		
		feature_row = feature_row[training_column_list] # reorder the columns to match
		
		# get prediction 1 or inlier, -1 for outlier
		prediction = clf.predict(feature_row)
		if (prediction == -1):  # an outlier
			absence_points = absence_points.append(feature_row,ignore_index=True)
			#should add absence points which are outliers
			print(absence_points) # currently printing
			counter += 1
		else:
			print("NOT AN OUTLIER")
			failed_absence_points = failed_absence_points.append(feature_row,ignore_index=True)
	
	return absence_points, failed_absence_points


def generate_all():
	from constants import TAXON_IDS
	for taxon_id in TAXON_IDS: # generate pseudo-absence for each of the species listed in constants.
		taxon_count = get_dataset_count(taxon_id) # 
        
		absence_points, failed_absence_points = generate_absence_points(taxon_count,taxon_id) 
		save_to_file = PROCESSED_DATASET / 'absence_{}_auto.csv'.format(taxon_id) # location of where to save
		absence_points.to_csv(save_to_file, index = None, header=True)
		save_to_file1 = PROCESSED_DATASET / 'failed_absence_{}_auto.csv'.format(taxon_id) # location of where to save
		failed_absence_points.to_csv(save_to_file1, index = None, header=True)
	return
	
if __name__ == "__main__": 
	taxon_id = 504391 # Just a test example
	absence_points, failed_absence_points = generate_absence_points(403,taxon_id) # 6573
    
    
	save_to_file = PROCESSED_DATASET / 'absence_{}_auto.csv'.format(taxon_id)
	absence_points.to_csv(save_to_file, index = None, header=True)
	save_to_file1 = PROCESSED_DATASET / 'failed_absence_{}_auto.csv'.format(taxon_id)
	failed_absence_points.to_csv(save_to_file1, index = None, header=True)
	
	
	
	
	
	
	
	
	
	
