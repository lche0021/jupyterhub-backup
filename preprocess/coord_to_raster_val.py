import os
from subprocess import check_output
from preprocess.coord_transform import transform_coord
from spectral import open_image
from preprocess.helper import pdf_to_coord


def _get_raster_value(coord, path_to_raster):
    """
    An auxiliary function that use these programs to obtain the raster value:
    - gdallocationinfo: A raster query tool
    - geoloc: Indicates input x,y points are in the georeferencing system of the image.
    - valonly: The only output is the pixel values of the selected pixel on each of the selected bands.
    :return: Raster value for each coordinate  
    """
    FNULL = open(os.devnull, 'w')
    cmd = ["/mnt/Anaconda3-2019.03-Linux-x86_64/envs/delwp/bin/gdallocationinfo", '-geoloc', '-valonly', str(path_to_raster), str(coord[0]), str(coord[1])]
    val = check_output(cmd, stderr=FNULL) 
    return val
  

# work for one band, but we probably not need SummerPre1750Landsat75_300_900m
def get_raster_value(coord_seq, path_to_raster):
    """
    Obtaining coordinates from a band and obtain the raster value in 
    the auxiliary function _get_raster_value.
    :param coord_seq: Coordinate from the band.
    :param path_to_raster: Directory of the raster file
    :return: A list of raster values from a band.
    """
    img = open_image(path_to_raster.parent / (path_to_raster.name + '.hdr'))
    values = []
    for coord in coord_seq:
        values.append(_get_raster_value(coord, path_to_raster))
    return values
    
