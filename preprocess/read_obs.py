import pandas as pd
from pathlib import Path
from constants import OBS_CSV, OBS_COL, SUBMISSIONS
from preprocess.helper import lower_colname

def get_obs(obs_path=OBS_CSV, keep_cols=OBS_COL):
	"""
	Read the file containing observations and dropping unwanted columns.
	:return An observation file with attributes that is useful to be fitted 
	in the model.
	"""
	if obs_path.suffix == '.csv':
		raw_data = pd.read_csv(obs_path)
	elif obs_path.suffix == '.xls':
		raw_data = pd.read_excel(obs_path)
	else:
		raise Exception('unrecognized input format')
	data = raw_data.drop(keep_cols,axis='columns')
	data = lower_colname(data)
	return data

def get_submission(taxon_id):
	"""
	Read the file containing observations to be tested and drop unwanted columns.
	:return An observation file with attributes that is useful to be fitted 
	in the model.
	"""
	raw_data = pd.read_excel(SUBMISSIONS / str(taxon_id))  
	data = raw_data.drop(OBS_COL,axis='columns')
	data = lower_colname(data)
	return data
