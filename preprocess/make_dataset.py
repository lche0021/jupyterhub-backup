import numpy as np
import pandas as pd
import sys

from constants import PROCESSED_RASTER_DATASET, PROCESSED_SHAPE_DATASET, NORMALIZED_DATASET, SUBMISSIONS, PROCESSED_DATASET
from preprocess.transform_columns import apply_transform
from pathlib import Path
from preprocess.read_obs import get_obs
from preprocess.coord_to_raster_val import get_raster_value
from preprocess.coord_to_shp_val import get_shapefile_value
from preprocess.helper import pdf_to_coord
from preprocess.coord_transform import transform_coord
from preprocess.read_raster import get_raster_dataset
from preprocess.read_shapefile import get_shapefile_dataset



"""
this script is supposed to run when generating dataset 
a few assumptions are made 
please refer to README.md for more details
"""

def decode_cmd_gdal_output(x):
	"""
	param x: 
	This function takes one parameter x,
	and decodes it to Unicode. 
	"""
	x = x.split(b'\n')
	x = [i.decode("utf-8") for i in x]
	if x[-1] == '':
		x = x[:-1]
	return x

def decode_cmd_gdal_output_multi_band(x):
	"""
	
	"""
	out = []
	for i in x:
		out.append(decode_cmd_gdal_output(i))
	return np.array(out)


def create_raster_dataset(raster_dataset, coord_seq_transformed, output_dir=PROCESSED_RASTER_DATASET):
	"""
	This function extracts environmental data from raster based on the coordinates in new osbervation records
	:param input_file: new observations record, xls or csv format
	:param output_dir: path to output directory, it will be created if not exist
	"""    
	for (raster_name, val) in raster_dataset.items(): #raster_dataset is not defined
		save_to_file = output_dir / '{}.csv'.format(raster_name)
		output_dir.mkdir(exist_ok=True)
		colnames = [raster_name+'_'+str(i) for i in range(val.nbands)]
		if not save_to_file.exists():
			print("creating raster dataset {}".format(save_to_file.name))
			raw_raster_values = get_raster_value(coord_seq_transformed, Path(val.filename))
			raster_values = decode_cmd_gdal_output_multi_band(raw_raster_values)
			raster_values_pds = pd.DataFrame(raster_values, columns=[colnames])
			raster_values_pds.to_csv(save_to_file, header=raster_name, index=False)
			print("raster dataset {} created".format(save_to_file.name))
		else:
			print("{} has been created before the run".format(raster_name))

			
def create_shapefile_dataset(shapefile_dataset, coord_seq_transformed, output_dir=PROCESSED_SHAPE_DATASET):
	"""
	This function extracts environmental data from shapefile based on the coordinates in new osbervation records
	:param input_file: new observations record, xls or csv format
	:param output_dir: path to output directory, it will be created if not exist
	"""    
	for (shapefile_name, val) in shapefile_dataset.items():
		save_to_file = output_dir / '{}.csv'.format(shapefile_name)
		output_dir.mkdir(exist_ok=True)
		if not save_to_file.exists():
			print("creating shapefile dataset {}".format(shapefile_name))
			raw_shapefile_values = get_shapefile_value(coord_seq_transformed, Path(shapefile_dataset[shapefile_name]))
			shapefile_values_pds = pd.DataFrame(raw_shapefile_values, columns=[shapefile_name])
			shapefile_values_pds.to_csv(save_to_file, header=shapefile_name, index=False)
			print("shapefile dataset {} is created".format(shapefile_name))
		else:
			print("{} has been created before the run".format(shapefile_name))
			


def make_dataset(input_file=SUBMISSIONS / 'submission.xls', output_dir=PROCESSED_DATASET, **kwargs):
	"""This function extracts environmental data from raster and shapefile dataset based on the coordinates in new osbervation records
	:param input_file: new observations record, xls or csv format
	:param output_dir: path to output directory, it will be created if not exist
	"""


	input_file = Path(input_file)
	try:
		assert input_file.exists() 
		obs_df = get_obs(obs_path = input_file)
		coord_seq = pdf_to_coord(obs_df)
		coord_seq_transformed = transform_coord(coord_seq)
		for coord in coord_seq_transformed: # handling of if coordinates are out of range
			if not 1970350 < coord[0] < 2991050:
				raise ValueError("longtitude {0} out of range".format(coord[0])) 
			if not 2258025 < coord[1] < 2948575:
				raise ValueError("latitude {0} out of range".format(coord[1]))
                
		shapefile_dataset = get_shapefile_dataset()
		raster_dataset = get_raster_dataset()
		output_dir.mkdir(exist_ok=True)
		create_shapefile_dataset(shapefile_dataset, coord_seq_transformed, output_dir = output_dir / 'shape')
		create_raster_dataset(raster_dataset, coord_seq_transformed, output_dir = output_dir / 'raster')
		return
	except AssertionError:
		print("Input file does not exist, please make sure inputted file is in correct location")
		sys.exit(0)
	except ValueError:
		print("A coordinate is outside the allowed range")
		sys.exit(0)

if __name__ == "__main__": 
	try:
		assert input_file.exists() 
		obs_df = get_obs()

		coord_seq = pdf_to_coord(obs_df)
		coord_seq_transformed = transform_coord(coord_seq)
		for coord in coord_seq_transformed:
			if not 1970350 < coord[0] < 2991050:
				raise Exception("longtitude {0} out of range".format(coord[0])) 
			if not 2258025 < coord[1] < 2948575:
				raise Exception("latitude {0} out of range".format(coord[1]))
		raster_dataset = get_raster_dataset()
		shapefile_dataset = get_shapefile_dataset()
		create_raster_dataset(raster_dataset, coord_seq_transformed)
		create_shapefile_dataset(shapefile_dataset, coord_seq_transformed)
		normalized_dataset = apply_transform()
		normalized_dataset.to_csv(NORMALIZED_DATASET, index=False)    
	except AssertionError:
		print("Input file does not exist, please make sure inputted file is in correct location")
		sys.exit(0)
	except ValueError:
		print("A coordinate is outside the allowed range")
		sys.exit(0)

