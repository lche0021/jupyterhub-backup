import os
import math
from osgeo import ogr
from preprocess.coord_transform import transform_coord
from preprocess.helper import pdf_to_coord, generate_point


def _get_shapefile_value(coord, layer):
    """
    A function which takes a coordinate pair and an open shapefile
    Returns the value at that given coordinate whether it be
        - a boolean indicating it is the given property of the shapefile
        - the distance to the cloest polygon of the type contained
          within the shapefile
          
    :param: coordinate - takes a coordinate pair in VicGrid94 format
    :param: shpfile - an open .shp file
    :return: the value at a coordinate 
    """
    newPoint = generate_point(coord)   # generates a point object with geometry

    contained_within = math.inf   # the checker
    for feature in layer:
        geometry = feature.GetGeometryRef()
        if (newPoint.Within(geometry)):   # point within polygon
            return 0
        else:   # distance to polygon
            dis_to_poly = newPoint.Distance(geometry)
            contained_within = min(dis_to_poly, contained_within)
    return contained_within

def get_shapefile_value(coord_seq, path_to_shapefile):
    """
    Takes a list of coordinates and returns a list of the values at each coordinate.
    :param: coord_seq - list of coordinates in VicGrid94 format
    :param: shpfile - the path to a shapefile
    :return: the list of values for each coordinate in coord_seq
    """
    driver = ogr.GetDriverByName('ESRI Shapefile')
    shpfile = driver.Open(str(path_to_shapefile.parent/
                              (path_to_shapefile.name+'.shp')),0)
    values = []
    layer = list(shpfile.GetLayer())
    for coord in coord_seq:
        shp_value = _get_shapefile_value(coord,layer)
        values.append(shp_value)
    return values
    
    
    
    
if __name__ == '__main__':
    from read_obs import get_obs
    from helper import pdf_to_coord
    obs = get_obs()
    coord = pdf_to_coord(obs)
    from pathlib import Path
    
    path_to_shapefile = Path('/mnt/dataset/shape/VMLITE_FOREST_SU2/VMLITE_FOREST_SU2')
    
    transformed_coord_lst = transform_coord(coord)
    import numpy as np 
    values = get_shapefile_value(transformed_coord_lst[0:10],path_to_shapefile)
    #values = get_shapefile_value(np.array([[2266008.3176857065, 2477311.3006592286 ]]),path_to_shapefile)

    print(values)

    
    
    
    
    
    
    
    
