import pandas as pd
from constants import PROCESSED_RASTER_DATASET
from preprocess.read_obs import get_obs
from preprocess.read_raster import get_raster_colnames
from preprocess.read_raster import get_processed_raster_dataset
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler


def define_raster_transforms():
    """
    """
    raster_columns = get_raster_colnames()
    column_trans = ColumnTransformer([
            ('standard_scaler', StandardScaler(copy=True, with_mean=True, with_std=True), raster_columns)
        ], remainder='drop')
    return column_trans

def apply_transform():
    """
    
    """
    obs_df = get_obs()
    raster_dataset_df = get_processed_raster_dataset()
    raster_transforms = define_raster_transforms()
    raster_dataset = pd.DataFrame(raster_transforms.fit_transform(raster_dataset_df), columns=raster_dataset_df.columns)
    dataset_df = pd.concat([obs_df, raster_dataset], axis='columns')
    return dataset_df
