import pandas as pd
from pathlib import Path
from constants import OBS_CSV, OBS_COL
from preprocess.helper import lower_colname

def get_species_obs(taxon_id):
    """
    A function which takes a taxon_id and returns a dataframe of the coordinates of the given species
    Deletes all of the other cols and filters the rows based on taxon_id
    return: the coordinates of a specific observation
    """
    data = pd.read_excel(OBS_CSV)  


    data = data[data.TAXON_ID == taxon_id]

    data.drop(data.columns.difference(['LATITUDEDD_NUM','LONGITUDEDD_NUM']), 1, inplace=True)

    columnOrder = ['LONGITUDEDD_NUM', 'LATITUDEDD_NUM']

    data = data.reindex(columns=columnOrder)

    data = lower_colname(data)
    
    return data
    