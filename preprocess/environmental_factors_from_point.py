import numpy as np
import pandas as pd

from pathlib import Path
from constants import PROCESSED_RASTER_DATASET, PROCESSED_SHAPE_DATASET, NORMALIZED_DATASET
from preprocess.coord_to_raster_val import get_raster_value
from preprocess.coord_to_shp_val import get_shapefile_value
from preprocess.coord_transform import transform_coord
from preprocess.helper import pdf_to_coord
from preprocess.read_obs import get_obs
from preprocess.read_raster import get_raster_dataset
from preprocess.read_shapefile import get_shapefile_dataset
from preprocess.transform_columns import apply_transform



"""
this script is supposed to run when generating dataset 
a few assumptions are made 
please refer to readme for more details
"""

def decode_cmd_gdal_output(x):
    """
    param x: 
    This function takes one parameter x,
    and decodes it to Unicode. 
    """
    x = x.split(b'\n')
    x = [i.decode("utf-8") for i in x]
    if x[-1] == '':
        x = x[:-1]
    return x

def decode_cmd_gdal_output_multi_band(x):
    """
    
    """
    out = []
    for i in x:
        out.append(decode_cmd_gdal_output(i))
    return np.array(out)


def create_raster_dataset(coord_seq_transformed, raster_dataset):
    """
    A function which generates the values from the raster files
    param: a sequence of coordinates in VicGrid94
    param: the raster dataset
    return: a dataframe containing the raster values
    """
    raster_values_pds = pd.DataFrame() # creates an empty dataframe
    for (raster_name, val) in raster_dataset.items():
        colnames = [raster_name+'_'+str(i) for i in range(val.nbands)]
        raw_raster_values = get_raster_value(coord_seq_transformed, Path(val.filename))
        raster_values = decode_cmd_gdal_output_multi_band(raw_raster_values)
        newColumn = pd.DataFrame(raster_values, columns=colnames)
        raster_values_pds = pd.concat([raster_values_pds, newColumn], axis=1) # merges features into column to be returned
#     print(raster_values_pds.columns.tolist())
    return raster_values_pds

            
def create_shapefile_dataset(coord_seq_transformed, shapefile_dataset):
    """
    A function which generates the values from the shapefiles
    param: a sequence of coordinates in VicGrid94
    param: the shapefile dataset
    return: a dataframe containing the shapefile values
    """
    shapefile_values_pds = pd.DataFrame() # creates an empty dataframe
    for (shapefile_name, val) in shapefile_dataset.items():
        raw_shapefile_values = get_shapefile_value(coord_seq_transformed, Path(shapefile_dataset[shapefile_name]))
        newColumn = pd.DataFrame(raw_shapefile_values, columns=[shapefile_name])
        shapefile_values_pds = pd.concat([shapefile_values_pds, newColumn], axis=1) # merges features into column to be returned
    return shapefile_values_pds
        
        
def environmental_factors_from_point(lat, long):
    """
    A function which generates the environmental values for a single set of coordinates
    param: latitude in vicgrid94
    param: longitude in vicgrid94
    return: the environmental at the coordinates
    """
    coord_seq = [[long,lat]]
    
    raster_dataset = get_raster_dataset()
    shapefile_dataset = get_shapefile_dataset()
#     print("length of shapefile dataset: " + str(len(shapefile_dataset))) 
    raster_vals = create_raster_dataset(coord_seq, raster_dataset)
    shape_vals = create_shapefile_dataset(coord_seq, shapefile_dataset)
#     print(raster_vals.columns.tolist())
#     print(shape_vals.columns.tolist())
    environmental_data = pd.concat([raster_vals, shape_vals], axis=1)
    return environmental_data

#if __name__ == "__main__": 
#    print("test")
    
