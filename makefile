generate_env_config: clean_config
	conda list --export > config/environment/requirements.txt
	cd config/environment && bash split_package_req.sh 
	cd ../..

install_packages:
	conda install -c conda-forge --file config/environment/conda_install_requirements.txt
	pip install -r config/environment/pip_install_requirements.txt

clean_config:
	rm -f config/environment/requirements.txt
	rm -f config/environment/pip_install_requirements.txt
	rm -f config/environment/conda_install_requirements.txt
