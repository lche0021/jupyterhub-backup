"""
This is a test code for obtaining shapefile value from .shp file.
VMLITE_FOREST_SU2.shp is used to obtain the shapefile value.
To test for other shapefile, change the name of the shapefile in the path
'/mnt/dataset/shape/VMLITE_FOREST_SU2/shapefile_name'
"""

from pathlib import Path
from preprocess.coord_to_shp_val import get_shapefile_value
from preprocess.coord_transform import transform_coord
from preprocess.helper import generate_point
from preprocess.helper import pdf_to_coord
from preprocess.read_obs import get_obs


if __name__ == "__main__":
    obs = get_obs()
    coord = pdf_to_coord(obs)
    path_to_shapefile = Path('/mnt/dataset/shape/VMLITE_FOREST_SU2/VMLITE_FOREST_SU2')
    transformed_coord_lst = transform_coord(coord)
    values = get_shapefile_value(transformed_coord_lst[0:10],path_to_shapefile)
    #values = get_shapefile_value(np.array([[2266008.3176857065, 2477311.3006592286 ]]),path_to_shapefile)
    print(values)
    
# Output: [10293.905001492447, 644.8457509369791, 927.6557998607351, 0, 735.085644098407, 164.05106063058213, 1785.3731500944295, 16982.543207978404, 20093.58399579781, 2067.462318411803]