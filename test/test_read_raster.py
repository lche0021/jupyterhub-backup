from preprocess.read_raster import get_raster_dataset, get_processed_raster_dataset


if __name__ == "__main__":
    print(get_raster_dataset())
    #print(get_processed_raster_dataset())

"""
Output:
{'vegtype3_4':  Data Source:   '/mnt/code/dataset/raster/vegtype3_4/vegtype3_4'        # Rows:           9234        # Samples:       13636        # Bands:             1        Interleave:        BIL
        Quantization:  16 bits
        Data format:     int16, 'wetness_index_saga_sept2012':  Data Source:   '/mnt/code/dataset/raster/wetness_index_saga_sept2012/wetness_index_saga_sept2012'
        # Rows:           9234
        # Samples:       13636        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits        Data format:   float32, 'sept2014JanRainfall':  Data Source:   '/mnt/code/dataset/raster/sept2014JanRainfall/sept2014JanRainfall'
        # Rows:           9234        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'hydro500xwi':  Data Source:   '/mnt/code/dataset/raster/hydro500xwi/hydro500xwi'
        # Rows:          26827
        # Samples:       39616
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'ecoregion1750':        Data Source:   '/mnt/code/dataset/raster/ecoregion1750/ecoregion1750'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  16 bits
        Data format:     int16, 'Anisotrophic_Heating_Ruggedness':      Data Source:   '/mnt/code/dataset/raster/Anisotrophic_Heating_Ruggedness/Anisotrophic_Heating_Ruggedness'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'SummerPre1750Landsat75_300_900m':      Data Source:   '/mnt/code/dataset/raster/SummerPre1750Landsat75_300_900m/SummerPre1750Landsat75_300_900m'
        # Rows:           9381
        # Samples:       13799
        # Bands:             8
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'ibra_hex':     Data Source:   '/mnt/code/dataset/raster/ibra_hex/ibra_hex'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:   8 bits
        Data format:     uint8, 'ecoregion2014':        Data Source:   '/mnt/code/dataset/raster/ecoregion2014/ecoregion2014'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  16 bits
        Data format:     int16, 'land_cov_use3':        Data Source:   '/mnt/code/dataset/raster/land_cov_use3/land_cov_use3'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  16 bits
        Data format:     int16, 'Radiometrics_2014_k':  Data Source:   '/mnt/code/dataset/raster/Radiometrics_2014_k/Radiometrics_2014_k'
        # Rows:           8360
        # Samples:       12173
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'Radiometrics_2014_th':         Data Source:   '/mnt/code/dataset/raster/Radiometrics_2014_th/Radiometrics_2014_th'
        # Rows:           8360
        # Samples:       12173
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, '75m_dem_streams_burned_sept2012':      Data Source:   '/mnt/code/dataset/raster/75m_dem_streams_burned_sept2012/75m_dem_streams_burned_sept2012'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'log_vertical_distance_saline_wetlands_sept2012':  Data Source:   '/mnt/code/dataset/raster/log_vertical_distance_saline_wetlands_sept2012/log_vertical_distance_saline_wetlands_sept2012'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'sept2014JulRainfall':  Data Source:   '/mnt/code/dataset/raster/sept2014JulRainfall/sept2014JulRainfall'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'sept2014JanMaxTemp':   Data Source:   '/mnt/code/dataset/raster/sept2014JanMaxTemp/sept2014JanMaxTemp'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'sept2014JulMinTemp':   Data Source:   '/mnt/code/dataset/raster/sept2014JulMinTemp/sept2014JulMinTemp'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'SummerLandsat75_300_900m':     Data Source:   '/mnt/code/dataset/raster/SummerLandsat75_300_900m/SummerLandsat75_300_900m'
        # Rows:           9381
        # Samples:       13800
        # Bands:             8
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32, 'ProtectionIndex':      Data Source:   '/mnt/code/dataset/raster/ProtectionIndex/ProtectionIndex'
        # Rows:           9234
        # Samples:       13636
        # Bands:             1
        Interleave:        BIL
        Quantization:  32 bits
        Data format:   float32}
       SummerPre1750Landsat75_300_900m_0  ...  sept2014JanMaxTemp_0
0                            2000.846680  ...            317.333099
1                            1854.276855  ...            270.901093
2                            1946.211182  ...            269.644501
3                            1682.266846  ...            269.376587
4                            1718.208130  ...            270.549805
5                            1896.152100  ...            316.264587
6                            2018.202637  ...            305.889008
7                            2000.215332  ...            319.936188
8                            2025.952026  ...            338.463013
9                            2073.529297  ...            265.120697
10                           2324.534668  ...            251.689102
11                           2324.534668  ...            251.689102
12                           2324.534668  ...            251.689102
13                           2330.620117  ...            247.929901
14                           2330.620117  ...            247.929901
15                           2330.620117  ...            247.929901
16                           1913.457886  ...            296.584290
17                           2184.114258  ...            298.262512
18                           2184.114258  ...            298.262512
19                           2184.114258  ...            298.262512
20                           2184.114258  ...            298.262512
21                           2184.114258  ...            298.262512
22                           2184.114258  ...            298.262512
23                           2120.821289  ...            285.331787
24                           1657.498047  ...            342.570190
25                           2104.345215  ...            264.478912
26                           1809.919189  ...            319.777588
27                           2284.720947  ...            356.696014
28                           2252.876465  ...            296.735504
29                           2252.876465  ...            296.735504
...                                  ...  ...                   ...
18860                        2084.141113  ...            269.432098
18861                        1947.563965  ...            258.539490
18862                        1736.327393  ...            286.823212
18863                        1823.263184  ...            262.925110
18864                        2196.970215  ...            226.861206
18865                        1789.404541  ...            261.241699
18866                        1797.787476  ...            270.237091
18867                        2061.558350  ...            311.117493
18868                        2061.558350  ...            311.117493
18869                        2177.085205  ...            266.323212
18870                        2204.042236  ...            285.247406
18871                        1970.301636  ...            316.618011
18872                        1876.321533  ...            305.729614
18873                        2059.179199  ...            250.620499
18874                        2220.447754  ...            286.546387
18875                        2201.105469  ...            291.542389
18876                        1958.308105  ...            268.167603
18877                        2046.846191  ...            287.080292
18878                        2195.631104  ...            264.265411
18879                        1857.517822  ...            268.433807
18880                        2175.144043  ...            278.481506
18881                        2012.830322  ...            277.613007
18882                        2230.759033  ...            288.222992
18883                        1936.561523  ...            313.993195
18884                        1954.651978  ...            346.493103
18885                        2009.607544  ...            253.537796
18886                        1769.679077  ...            248.313400
18887                        1903.690674  ...            294.952393
18888                        1976.756348  ...            295.556885
18889                        2103.470947  ...            281.724304

[18890 rows x 33 columns]
"""