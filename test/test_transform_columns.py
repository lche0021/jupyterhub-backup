from preprocess.transform_columns import apply_transform

if __name__ == "__main__":
    print(apply_transform())
    
"""
           ufi  taxon_id  ... SummerLandsat75_300_900m_7  sept2014JanMaxTemp_0
0       613779    503998  ...                   1.183180              0.568542
1      2001011    504391  ...                  -1.207857             -0.771037
2      2001732    504391  ...                  -0.440009             -0.807290
3      2001967    504391  ...                   0.513300             -0.815019
4      2002392    504391  ...                   0.284114             -0.781172
5      2655033    504391  ...                   1.249466              0.537716
6      3892275     60555  ...                   0.809493              0.238377
7      3913538     60555  ...                   0.849474              0.643642
8      3920111     60555  ...                   0.871131              1.178148
9      3922496     13182  ...                  -0.080958             -0.937803
10     3923667     11028  ...                  -0.584198             -1.325309
11     3923610     13182  ...                  -0.584198             -1.325309
12     3923643     13182  ...                  -0.584198             -1.325309
13     3923874     11028  ...                  -0.525514             -1.433763
14     3923874     11028  ...                  -0.525514             -1.433763
15     3923824     13182  ...                  -0.525514             -1.433763
16     3926167     11028  ...                   0.855209             -0.030068
17     3929575     11028  ...                   0.297983              0.018350
18     3929575     11028  ...                   0.297983              0.018350
19     3929576     11028  ...                   0.297983              0.018350
20     3929576     11028  ...                   0.297983              0.018350
21     3929574     11028  ...                   0.297983              0.018350
22     3929574     11028  ...                   0.297983              0.018350
23     3932253     60555  ...                   0.533358             -0.354706
24     3942343     60555  ...                  -0.455292              1.296641
25     3944929     13182  ...                   0.212790             -0.956319
26     3964549     60555  ...                   0.449459              0.639067
27     3974071     60555  ...                  -1.976379              1.704176
28     3978675     13182  ...                  -0.205208             -0.025705
29     3978675     13182  ...                  -0.205208             -0.025705
...        ...       ...  ...                        ...                   ...
18860  8610940     11028  ...                  -1.132424             -0.813418
18861  8610967     11028  ...                  -1.585919             -1.127673
18862  8613532     11028  ...                  -0.694545             -0.311678
18863  8614808     11028  ...                  -1.771003             -1.001147
18864  8615004     11028  ...                  -1.075392             -2.041602
18865  8615456     11028  ...                  -0.613993             -1.049714
18866  8655585     11028  ...                  -1.750683             -0.790193
18867  8710418     13182  ...                   0.124912              0.389220
18868  8710427     13182  ...                   0.124912              0.389220
18869  8744835     11028  ...                   0.677551             -0.903110
18870  8690485     11028  ...                   0.621251             -0.357141
18871  8898754     60555  ...                   0.883463              0.547912
18872  8913132    504391  ...                   0.805999              0.233778
18873  8922725    504391  ...                  -0.335998             -1.356139
18874  8948200     11028  ...                   0.891951             -0.319665
18875  8948380     11028  ...                   0.378936             -0.175528
18876  8954129     11028  ...                  -0.659547             -0.849899
18877  8957973     11028  ...                  -0.935378             -0.304261
18878  8960032     11028  ...                  -0.871463             -0.962478
18879  8994714     11028  ...                  -1.602442             -0.842219
18880  9004757     11028  ...                  -1.337777             -0.552339
18881  9005847     11028  ...                  -0.383559             -0.577396
18882  9010586     13182  ...                   1.053295             -0.271294
18883  9012909     60555  ...                   0.549318              0.472185
18884  9014202     10561  ...                   0.367458              1.409819
18885  9021533     11028  ...                  -1.545942             -1.271974
18886  9021559     11028  ...                  -1.904246             -1.422699
18887  9025887     11028  ...                   0.525567             -0.077149
18888  9041530     11028  ...                   0.541527             -0.059709
18889  9041868     11028  ...                  -1.013742             -0.458783

[18890 rows x 49 columns]
"""