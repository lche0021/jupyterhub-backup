from preprocess.coord_to_raster_val import get_raster_value
from preprocess.coord_transform import transform_coord
from preprocess.read_obs import get_obs
from preprocess.helper import pdf_to_coord
from pathlib import Path
        

if __name__ == "__main__":
    obs = get_obs()
    coord = pdf_to_coord(obs)
    path_to_sample_raster = Path('/mnt/dataset/raster/SummerPre1750Landsat75_300_900m/SummerPre1750Landsat75_300_900m')
    transformed_coord_list = transform_coord(coord)
    values = get_raster_value(transformed_coord_list[:10], path_to_sample_raster)
    print(values)
