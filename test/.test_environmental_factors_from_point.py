from preprocess.environmental_factors_from_point import environmental_factors_from_point


if __name__ == "__main__":
    # Test for presence data
    print(environmental_factors_from_point(lat=-37.00972,long=142.72417))
    # Test for absence data
    print(environmental_factors_from_point(lat=-38.062220,long=147.557220))
    # The latitude and longitude are taken from one of the observations
#Test status: fail