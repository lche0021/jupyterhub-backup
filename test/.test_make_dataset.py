from preprocess.coord_transform import transform_coord
from preprocess.helper import pdf_to_coord
from preprocess.make_dataset import create_raster_dataset, create_shapefile_dataset
from preprocess.read_obs import get_obs
from preprocess.read_raster import get_raster_dataset
from preprocess.read_shapefile import get_shapefile_dataset
from preprocess.transform_columns import apply_transform


def test_dataset():
    obs_df = get_obs()
    # for testing
    coord_seq = pdf_to_coord(obs_df)
    coord_seq_transformed = transform_coord(coord_seq)
    raster_dataset = get_raster_dataset()
    shapefile_dataset = get_shapefile_dataset()
    create_raster_dataset()
    create_shapefile_dataset()
    normalized_dataset = apply_transform()
    normalized_dataset.to_csv(NORMALIZED_DATASET, index=False)
    

if __name__ == "__main__":
    test_dataset()