#!/bin/bash
for i in $(ls .)
do
    if [ "${i#*.}" = "py" ]; then
        echo "run $i"
        python $i
    fi
done
