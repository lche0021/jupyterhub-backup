import pandas as pd
from constants import NORMALIZED_DATASET, DATASET_DIR, OBS_CSV, PROCESSED_DATASET, SUBMISSIONS
from preprocess.read_raster import get_processed_raster_dataset
from preprocess.read_obs import get_obs
from preprocess.read_shapefile import get_processed_shapefile_dataset
from pathlib import Path
# this is the entry point for the rest of software to interact with processed dataset

def load_dataset(dataset_root=PROCESSED_DATASET, input_file=None):
	"""load processed dataset from dataset_root and input_file 
	:param dataset_root: the path to  root of dataset made by preprocess/make_dataset.py
	:param input_file: the xls or csv file that record new observations (default: SUBMISSIONS/submission.xls)
	"""
	dataset_root = Path(dataset_root)
	if input_file is not None:
		obs_df = get_obs(obs_path = input_file)
	else:
		obs_df = get_obs()
	raster_dataset_df = get_processed_raster_dataset(raster_dataset_dir = dataset_root / 'raster')
	shapefile_dataset_df = get_processed_shapefile_dataset(dataset_dir = dataset_root / 'shape')
	dataset_df = pd.concat([obs_df, raster_dataset_df, shapefile_dataset_df], axis='columns')
	# return an error indicate nan cannot be converted to int
	# dataset_df['rating_int'] = dataset_df['rating_int'].astype(int)
	return dataset_df

def load_normalized_dataset():
	return pd.read_csv(NORMALIZED_DATASET)


def load_observations_dataset(taxon_id):
	obs_df = get_submission(taxon_id)
	raster_dataset_df = get_processed_raster_dataset()
	shapefile_dataset_df = get_processed_shapefile_dataset()
	dataset_df = pd.concat([obs_df, raster_dataset_df, shapefile_dataset_df], axis='columns')
	return dataset_df
