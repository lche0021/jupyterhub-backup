# Instructions to run the model and code

## To run the code follow these instructions below
### Generating model for the first time
```
1. Ensure that in constants.py you have the OBS_CSV set to ‘Monash_sample_VBA.csv’ or whatever csv you are building the models off of.
2. Run the make_dataset.py, this may take a considerable amount of time as it has to scrap the extra values from the raster and shapefiles.
3. Next you will need to run make_model.py, this is a script which will handle the rest of the setup process with it building the absence data, which, like the make_dataset.py will take a while, followed by automatically running the model_generator.py which will actually build the models.
4. From here you will be ready to begin testing your models

```

### To test new observations
```
1. You are first required to ensure the OBS_CSV is set to SUBMISSIONS / ‘submissions.xls’ in constants.py .
2. You are required to place a .xls file formatted as ‘submission.xls’ into the submissions folder. 
3. You will then run tool.py, where you will be prompted with a few questions, including the taxon id of the species you are wanting to use.
4. An output file and a map with the new observations will be placed in the submissions folder which will be your original xls file with an extra column containing the estimated probabilities.
5. Whenever you add a new submission, you will need to make sure that the raster and shapefiles in testing/output/ are empty before you make the dataset.

```
