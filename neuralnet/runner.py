#!/usr/bin/env python
from fire import Fire
import numpy as np
import tensorflow as tf
from preprocess.read_raster import get_raster_colnames
from constants import SHAPEFILE_COL
from neuralnet.train import train_model
import pandas as pd
from constants import SUBMISSIONS, ABSENCE_DATASET
from preprocess.make_dataset import make_dataset
from load_dataset import load_dataset
from pathlib import Path
from neuralnet.feature_engineering import create_feature_columns
MODEL_DIR = Path(__file__).resolve().parent / 'saved_model'

def load_parsed_ds(taxon_id, use_feature, absence=False, **kwargs):
	dataset_df = load_dataset(**kwargs)
	dataset_df[['park_id', 'rating_int']] = dataset_df[['park_id', 'rating_int']].fillna(-1)
	dataset_df[['park_id', 'rating_int']] = dataset_df[['park_id', 'rating_int']].astype(int)
	strcol = dataset_df.select_dtypes('object').columns
	dataset_df[strcol] = dataset_df[strcol].fillna('other')
	dataset_df = dataset_df[dataset_df['taxon_id'] == taxon_id].reset_index(drop=True)
	label_ds = pd.Series(data=np.ones(len(dataset_df.index)))
	dataset_df = dataset_df[use_feature]
	if absence:
		negative_df  = pd.read_csv(ABSENCE_DATASET /  (str(taxon_id) + '.csv'))[use_feature]
		dataset_df = pd.concat([dataset_df, negative_df])
		label_ds = pd.concat([label_ds, pd.Series(data=np.zeros((len(negative_df))))])

		return dataset_df, label_ds
	else:
		return dataset_df
def generate_config(taxon_id, **kwargs):
	config = {
		'train_test_split': 0.2,
		'batch_size': 32,
		'num_epoch': 5,
		'queue_capacity': 100,
		'num_threads': 1,
		'hidden_units': [1024, 512, 256],
		'checkpoint_dir': MODEL_DIR / str(taxon_id)
	}
	for _, (k, v) in enumerate(kwargs):
		if k in config.keys():
			config[k] = v
	return config
USE_FEATURE= get_raster_colnames() + SHAPEFILE_COL

def predict(x):
  example = tf.train.Example()
  example.features.feature["x"].float_list.value.extend([x])
  return imported.signatures["predict"](
    examples=tf.constant([example.SerializeToString()]))
# A utility method to create a tf.data dataset from a Pandas Dataframe
def df_to_dataset(dataframe, batch_size=32):
	ds = dataframe.copy()
	ds = tf.data.Dataset.from_tensor_slices((dict(dataframe)))
	ds = ds.batch(batch_size)
	return ds

def train(taxon_id, **kwargs):
	"""train a DNNClassifier
	:param taxon_id: the id of species
	:param **kwargs: anything in config can also be specified in CLI, it will overwrite default setting. (e.g. --batch_size=16)	
	"""
	data_df, label_ds = load_parsed_ds(taxon_id, USE_FEATURE, absence=True)
	config = generate_config(taxon_id, **kwargs)
	model, metric = train_model(data_df, label_ds, **config)
	return 'accuracy is : ' + str(metric['accuracy'])
def scoring(taxon_id, input_file = SUBMISSIONS / 'submission.xls', output_dir='./output_dir' ,**kwargs):
	"""scoring new observations
	:param taxon_id: determine which input file to use, input file will be read from SUBMISSIONS / sumission.xls
	"""	
	output_dir = Path(output_dir).resolve()
	input_file = Path(input_file)
	if input_file.exists():
		if (MODEL_DIR / str(taxon_id)).exists():
			make_dataset(input_file = input_file, output_dir = output_dir)
			dataset = load_parsed_ds(taxon_id, USE_FEATURE, dataset_root = output_dir, input_file = input_file)
			model_path = max([i for i in (MODEL_DIR / str(taxon_id)).iterdir() if i.is_dir() and i.name.isdigit()], key=lambda x : int(x.name))
			model = tf.saved_model.load(str(model_path))

			prediction_input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(dataset, shuffle=False)
			predict = model.signatures['serving_default']
			feature_columns = create_feature_columns(dataset)
			feature_spec = tf.feature_column.make_parse_example_spec(feature_columns)	
			#ds = tf.data.Dataset.from_tensor_slices(dict(dataset))
			
			for _, val in dataset.iterrows():
				feature = list(val)
				feature = dict(zip(list(dataset.columns), feature))
				for k in list(feature.keys()):
					feature[k] = [feature[k]]
				predict([feature])
			
		else:
			raise Exception("the model haven't been trained, please run training first")	
		
	else:
		raise Exception("the input file does not exist")

if __name__ == "__main__":
	Fire({'train': train, 'scoring': scoring, 'make_dataset': make_dataset, 'load_dataset': load_dataset})
