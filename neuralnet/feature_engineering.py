import tensorflow as tf
from preprocess.read_raster import get_raster_colnames
import numpy as np
from constants import SHAPEFILE_COL
from tensorflow import feature_column


def create_feature_columns(df):
    GEOLOC_CROP_SIZE=10
    columns = []
    raster_colnames = get_raster_colnames()
    
#   categrical features
    # add reliability to feature col later
    for colname in ['taxon_id', 'reliability_txt', 'record_type', 'creation_tsp', 'sampling_method_desc']:
        if colname in df.columns:
            columns.append(
                feature_column.indicator_column(feature_column.categorical_column_with_vocabulary_list(
                    key=colname,
                    vocabulary_list=list(df[colname].unique())
                )))
#         print(list(df[colname].unique()))
#   float valued categrical features, they cannot be directly converted to int due to nan value
#   i just use -1 to substitute nan here
    for colname in ['park_id', 'rating_int']:
        if colname in df.columns:
            columns.append(
                feature_column.indicator_column(feature_column.categorical_column_with_identity(
                    key=colname, 
                    num_buckets=max(df[colname])+1
                ))
            )
        
#   numeric columns
    for colname in ['sv_record_count'] + raster_colnames + SHAPEFILE_COL:
        if colname in df.columns:
            columns.append(
                feature_column.numeric_column(
                    key=colname
                )
            )

# only for geolocation columns, 
    if 'latitudedd_num' in df.columns and 'longitudedd_num' in df.columns:
        geoloc_col = feature_column.crossed_column(
                keys=[
                    feature_column.bucketized_column(
                        feature_column.numeric_column(key='latitudedd_num'),
                        boundaries=list(np.linspace(
                            start=df['latitudedd_num'].min(), stop=df['latitudedd_num'].max(), num=GEOLOC_CROP_SIZE).astype(np.float32)
                        )),
                    feature_column.bucketized_column(
                        feature_column.numeric_column(key='longitudedd_num'),
                        boundaries=list(np.linspace(
                            start=df['longitudedd_num'].min(), stop=df['longitudedd_num'].max(), num=GEOLOC_CROP_SIZE).astype(np.float32)
                        ))
                ],
                hash_bucket_size=GEOLOC_CROP_SIZE * GEOLOC_CROP_SIZE
            )
        geoloc_col = feature_column.embedding_column(geoloc_col, 1) 
        columns.append(geoloc_col)
    return columns


