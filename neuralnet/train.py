#!/usr/bin/env python
import tensorflow as tf 
from neuralnet.feature_engineering import create_feature_columns
from tensorflow.estimator import DNNClassifier
from sklearn import model_selection 
import pandas as pd
from preprocess.read_raster import get_raster_colnames
from constants import ABSENCE_DATASET
import numpy as np
from datetime import datetime
import multiprocessing
from constants import RASTER_COL, SHAPEFILE_COL


def input_fn(data_train, label_train, data_test, label_test, **kwargs):
	train_input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(
		data_train,
		y=label_train,
		batch_size=kwargs['batch_size'],
		num_epochs=kwargs['num_epoch'],
		shuffle=True,
		queue_capacity=kwargs['queue_capacity'],
		num_threads=kwargs['num_threads']
	)
	test_input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(
		data_test,
		y=label_test,
		shuffle=False
	)
	return train_input_fn, test_input_fn
	

def create_estimator(feature_columns, **config):
	estimator = DNNClassifier(
		feature_columns = feature_columns,
		hidden_units=config['hidden_units'],
		model_dir=config['checkpoint_dir']
	)
	return estimator

def train_model(data, label, **kwargs):
	data_train, data_test, label_train, label_test = model_selection.train_test_split(data, label, test_size=kwargs['train_test_split'], shuffle=True)
	train_input_fn, test_input_fn = input_fn(data_train, label_train, data_test, label_test, **kwargs)
	feature_columns = create_feature_columns(data_train)
	est = create_estimator(feature_columns, **kwargs)
	model = est.train(input_fn=train_input_fn)
	eval_matric = model.evaluate(test_input_fn)
	serving_input_fn = tf.estimator.export.build_parsing_serving_input_receiver_fn(tf.feature_column.make_parse_example_spec(feature_columns))
	model.export_saved_model(str(kwargs['checkpoint_dir']), serving_input_fn)

	return model, eval_matric 

if __name__ == '__main__':
	data_df, label_ds = load_data(11028, get_raster_colnames() + SHAPEFILE_COL)
	data_df.reset_index(inplace=True, drop=True), label_ds.reset_index(inplace=True, drop=True)
	data_train, data_test, label_train, label_test = model_selection.train_test_split(data_df, label_ds, test_size=0.2, shuffle=False)

	train_input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(
		data_train,
		y=label_train,
		batch_size=32,
		num_epochs=5,
		shuffle=True,
		queue_capacity=1000,
		num_threads=1,
	)
	test_input_fn = tf.compat.v1.estimator.inputs.pandas_input_fn(
		data_test, 
		y=label_test,
		shuffle=False
	)
	feature_columns = create_feature_columns(data_train)
	estimator = DNNClassifier(
		feature_columns = feature_columns, 
		hidden_units=[1024, 512, 256], 
		model_dir='./logs/'
	#	 optimizer = 'adam'
	)
	model = estimator.train(input_fn=train_input_fn)
	model.evaluate(test_input_fn, steps=10)

