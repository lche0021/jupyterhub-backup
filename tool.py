import sys
sys.path.append("/mnt/code/")

import os
import math
import pandas as pd
import random
import numpy as np

from osgeo import ogr
from preprocess.coord_transform import transform_coord
from preprocess.helper import pdf_to_coord
from preprocess.helper import generate_point 
from pathlib import Path
from constants import OBS_CSV, OBS_COL
from preprocess.helper import lower_colname
from load_dataset import load_dataset
from preprocess.environmental_factors_from_point import environmental_factors_from_point
from constants import PROCESSED_DATASET, SUBMISSIONS

def transform_coords(dataframe):
    ## Here we need to transform the columns of the dataset so they are in VicGrid94
    ## Assumes latitude and longitude are uppercase
    column_data = dataframe[['latitudedd_num'.upper(),'longitudedd_num'.upper()]]
    columnOrder = ['longitudedd_num'.upper(), 'latitudedd_num'.upper()]
    column_data = column_data.reindex(columns=columnOrder)
    transformed_coord_lst = transform_coord(column_data)
    new_columns = pd.DataFrame({'longitudedd_num'.upper(): transformed_coord_lst[:, 0], 'latitudedd_num'.upper(): transformed_coord_lst[:, 1]})
    dataframe[['latitudedd_num'.upper()]] = new_columns[['latitudedd_num'.upper()]]
    dataframe[['longitudedd_num'.upper()]] = new_columns[['longitudedd_num'.upper()]]
    return dataframe

def plot_observations(taxon_id):
    ## We first need to get the shapefile that holds the map of Victoria
    from constants import SUBMISSIONS
    from constants import SHAPEFILE_DIR
    import geopandas as gpd
    import matplotlib.pyplot as plt
    vic_map_link = SHAPEFILE_DIR / 'VMLITE_VICTORIA_POLYGON_SU5/VMLITE_VICTORIA_POLYGON_SU5.shp'
    vic_map = gpd.read_file(vic_map_link)
    
    ## We then need to get the coordinates of a certain species
    from constants import DATASET_DIR
    presence = pd.read_excel(DATASET_DIR / 'obs' / 'Monash_sample_VBA.xls')
    presence = presence[presence.TAXON_ID == int(taxon_id)]
    presence = presence.reset_index(drop=True)
    presence = transform_coords(presence)
    
    ## We can now get the new observations
    from constants import SUBMISSIONS
    path_to_submission = SUBMISSIONS / '{}_ouput.csv'.format(taxon_id)
    new_observations = pd.read_csv(path_to_submission)
    new_observations = transform_coords(new_observations)
    
    ## Now that we have all the observations we need to make them into geometry
    from shapely.geometry import Point, Polygon
    crs = {'init': 'epsg:3111'}
    geometry = [Point(xy) for xy in zip( presence['longitudedd_num'.upper()],presence['latitudedd_num'.upper()] )]
    geo_presence = gpd.GeoDataFrame(presence, crs = crs, geometry = geometry)
    
    new_obs_geo = [Point(xy) for xy in zip( new_observations['longitudedd_num'.upper()],new_observations['latitudedd_num'.upper()] )]
    geo_new = gpd.GeoDataFrame(new_observations, crs = crs, geometry = new_obs_geo)
    
    ## Lastly we need to actually plot and save the points
    fig, ax = plt.subplots(figsize = (15,15))
    ax.set_title('Species Points {}'.format(taxon_id), fontsize=16)
    vic_map.plot(ax = ax) # Plot Victoria
    
    y2 = list(geo_presence['longitudedd_num'.upper()])
    z2 = list(geo_presence['latitudedd_num'.upper()])
    ax.scatter(y2,z2, color='yellow', label = "Presence") # plot the presence
    
    y = list(geo_new[geo_new['reliable_prob'] > 0.5]['longitudedd_num'.upper()])
    z = list(geo_new[geo_new['reliable_prob'] > 0.5]['latitudedd_num'.upper()])
    n = list(geo_new[geo_new['reliable_prob'] > 0.5]['UFI'])
    nn = list(geo_new[geo_new['reliable_prob'] > 0.5]['reliable_prob'])
    nnew = [(str(n[i]) + ' P:' + str(round(float(nn[i]),3))) for i in range(len(n))] 
    ax.scatter(y,z, color='orange', label = 'proba_reliable') # plot the 'reliable' points

    y1 = list(geo_new[geo_new['reliable_prob'] <= 0.5]['longitudedd_num'.upper()])
    z1 = list(geo_new[geo_new['reliable_prob'] <= 0.5]['latitudedd_num'.upper()])
    n1 = list(geo_new[geo_new['reliable_prob'] <= 0.5]['UFI'])
    nn1 = list(geo_new[geo_new['reliable_prob'] <= 0.5]['reliable_prob'])
    nnew1 = [(str(n1[i]) + ' P:' + str(round(float(nn1[i]),3))) for i in range(len(n1))] 
    ax.scatter(y1,z1, color='red', label = "proba_unreliable") # plot the 'unreliable' points

    
    plt.legend(prop={'size': 15})
    for i, txt in enumerate(nnew):
        ax.annotate(txt, (y[i], z[i]))

    for i, txt in enumerate(nnew1):
        ax.annotate(txt, (y1[i], z1[i]))

    fig.savefig(SUBMISSIONS / 'species_points_{}.png'.format(taxon_id), bbox_inches='tight')

def write_prediction(taxon_id, model):    
    # run programe
    handle_new_observation(model, taxon_id)
    plot_observations(taxon_id)
        
    return
        
def scale_dataset(model, dataset):
    from sklearn.preprocessing import StandardScaler
    
    transformer = StandardScaler().fit(dataset)
    transformer.transform(dataset)
    return dataset
        
def handle_new_observation(model,taxon_id):
    new_observations = load_dataset()
    new_observations = lower_colname(new_observations)
    
    try:
        if int(taxon) in list(new_observations['taxon_id']) == int(taxon_id):
            raise Exception("Please ensure that all species in submissions are of the required species.\n{} is not {}".format(taxon, taxon_id)) 
    except:
        print("Please ensure that all species in submissions are of the required species.")
        sys.exit(0)
    
    cols_to_drop = ['taxon_id', 'ufi', 'rating_int', 'survey_start_date', 'lat_long_accuracydd_int', 'park_id','survey_id','creation_tsp', 'reliability_txt']
    new_observations = new_observations.drop(cols_to_drop, 1)
    
    from preprocess.model_generator import generate_presence_column
    new_observations = generate_presence_column(new_observations)
    
    ## Here we need to transform the columns of the dataset so they are in VicGrid94
    column_data = new_observations[['latitudedd_num','longitudedd_num']]
    columnOrder = ['longitudedd_num', 'latitudedd_num']
    column_data = column_data.reindex(columns=columnOrder)
    transformed_coord_lst = transform_coord(column_data)
    new_columns = pd.DataFrame({'longitudedd_num': transformed_coord_lst[:, 0], 'latitudedd_num': transformed_coord_lst[:, 1]})
    new_observations[['latitudedd_num']] = new_columns[['latitudedd_num']]
    new_observations[['longitudedd_num']] = new_columns[['longitudedd_num']]
    
    
    # These are to be dropped from the dataset
    new_observations.drop(['sv_record_count', 'sampling_method_desc','record_type', 'reliability', 'total_count_int'], 1, inplace=True) 
    
    from preprocess.model_generator import load_model
    from preprocess.model_generator import make_predictions
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.ensemble import AdaBoostClassifier
    from sklearn.ensemble import GradientBoostingClassifier
    from sklearn.svm import SVC
    from sklearn.preprocessing import StandardScaler
    from sklearn.neural_network import MLPClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn import tree
    
    if model == "MLP_classifier":
        new_observations = scale_dataset("mlp", new_observations)
    elif model == "support_vector_classifier":
        new_observations = scale_dataset("svc", new_observations)

    test_model = load_model(model,str(taxon_id))
    predictions = test_model.predict_proba(new_observations)

    return_file = pd.read_excel(OBS_CSV)
    results = pd.DataFrame({'reliable_prob': predictions[:, 0], 'unreliable_prob': predictions[:, 1]})
    
    return_file = return_file.join(results)
    return_file.to_csv(SUBMISSIONS/'{}_ouput.csv'.format(taxon_id), sep=',')
    
    return




if __name__ == "__main__": 
    submission_type = input("Would you like to make a submission. \nPlease note that batch submissions must be in the assumed VBA format and that they are inside project/submissions\n y/n: ")
    if submission_type.lower() == "y":
        
        #### Handling the taxon choice ####
        taxon_id = input("Enter taxon id (for a list of taxon ids, enter 'list'): ")
        from constants import TAXON_IDS # contains list of taxon ids
        def print_taxons(): ## a small function to print the list of availiable taxon_ids
            for taxon in TAXON_IDS : print("Model: {}".format(taxon))
        
        if taxon_id == "list":
            print_taxons()
        
        taxons = TAXON_IDS
        for index, taxon in enumerate(taxons): taxons[index]= str(taxon)
        
        while taxon_id not in taxons:  ## ensure that the user picks an model correctly
            taxon_id = input("Invalid taxon choice, please enter a valid taxon: ")
            if taxon_id == "list" : print_taxons()
        
        #### Handling the model choice ####
        model_list = ["random_forest_model", "ada_boost", "gb_classifier", "kneigh_classifier", "tree_classifier", 'support_vector_classifier', 'MLP_classifier']
            
        
        model = input("What model would you like to use? (random_forest_model is recommended but for a list of choices type 'list'): ")
        
        def print_models(): ## a small function to print the list of availiable model choices
            for model in model_list : print("Model: {}".format(model))
                
        if model == "list":
            print_models()
            
        while model not in model_list:  ## ensure that the user picks an model correctly
            model = input("Invalid model choice, please enter one of the availiable models: ")
            if model == "list" : print_models()
                
        #### Begin the process of actually completing the submission ####
        from preprocess.make_dataset import make_dataset
        make_dataset()
        
        ## create the model prediction
        write_prediction(taxon_id, model)
        
    elif submission_type.lower() == "n":
        print("Nevermind then")
        
    else:
        print("Unknown command")
        
        
        
                            
          
                           