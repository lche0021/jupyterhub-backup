from setuptools import setup, find_packages
setup(
    name = 'reliability_classifier',
    version = '0.1',
    description = 'This is our final software product for FIT3162 Comptuer Science Project, Monash University',
    author = ['Luhan Cheng','Magnus Bradley','Muhammad Shahmil'], 
    author_email = ['lche0021@student.monash.edu','mobra1@student.monash.edu','mssha24@student.monash.edu'], 
    packages = find_packages(),
    requires = []
)
