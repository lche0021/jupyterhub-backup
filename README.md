# Reliability Classifier

This is a DELWP project to determine the reliability of the model predicting the accuracy of observations of flora and fauna in Victoria.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Need anaconda3 to run software

### Installing

run following commands, replace PROJECT_ROOT with the root directory of code, replace PROJECT_NAME as the name of virtual environment
```
conda env create -f environment.yml
source activate reliability_classifier
```
this will create a anaconda environment that contains all required packages

In order to run the tool.py. You need to make the top level of project structure visible to python. We recommand you add following command to your ~/.bashrc

```
export PYTHONPATH=<ROOT>:${PYTHONPATH}
```
where you need to substitute <ROOT> to path to the software
and run 
```
source ~/.bashrc
```

### Dataset preparation

The  dataset is hosted on google drive. We were using the script dataset_process.sh in example directory to preprocess data. This script will recursively unzip all zip file. and pul unpacked dataset at d/mnt/dataset with the classification of raster, shape, observation and run this script several times until all zips have been unziped. 



A csv file containing observations from various places was processed by taking these steps. First, the problem in the dataset has to be identified. Second, balancing data is needed if the dataset is unbalanced. Finally, preprocessing data is done for data cleaning purposes. 

#### Dataset description 

The orignal dataset is hosted on Google Drive, consist of three source of data : raster dataset, shapefile dataset and observation records. The raster dataset contains information about the bands, file type, data type, coordinate format and etc. The shapefile dataset contains attributes of the shapefile.

| UFI | TAXON_ID | RELIABILITY | RATING_INT | RELIABILITY_TXT | SAMPLING_METHOD_DESC | SURVEY_START_DATE | LATITUDEDD_NUM | LONGITUDEDD_NUM | LAT_LONG_ACCURACYDD_INT | TOTAL_COUNT_INT | RECORD_TYPE | SV_RECORD_COUNT | PARK_ID | SURVEY_ID | CREATION_TSP |
| :--: | :------: | :--------: | :---: | :--------: | :---: | :-------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 0 | 613779 | 503998 | Acceptable | 0.0 | NaN | Quadrat | 13/OCT/1988 | -37.00972 | 142.72417 | 15 | NaN | NaN | 113 | 3396.0 | 246005 | 16/AUG/11 12:00:00.000000000 AM
| 1 | 2001011 | 504391 | Acceptable | 0.0 | NaN | Quadrat | 16/FEB/1999 | -38.06222 | 147.55722 | 15 | NaN | NaN | 23 | NaN | 392381 | 16/AUG/11 12:00:00.000000000 AM
| 2 | 2001732 | 504391 | Acceptable | 0.0 | NaN | Quadrat | 12/MAY/1999 | -38.05917 | 147.55528 | 15 | NaN | NaN | 27 | NaN | 392411 | 16/AUG/11 12:00:00.000000000 AM
| 3 | 2001967 | 504391 | Acceptable | 0.0 | NaN | Quadrat | 13/MAY/1999 | -38.15000 | 147.42250 | 15 | NaN | NaN | 32 | 3362.0 | 392419 | 16/AUG/11 12:00:00.000000000 AM
| 4 | 2002392 | 504391 | Acceptable | 0.0 | NaN | Quadrat | 15/OCT/1999 | -38.07500 | 147.53083 | 15 | NaN | NaN | 35 | 3362.0 | 392434 | 16/AUG/11 12:00:00.000000000 AM |

#### Dataset generation
In project/preprocess directory, there is a Python script make_dataset.py which contains several functions that generate dataset from raster files and shapefiles. Then, the dataset is normalized before written into .csv file.


## Running the tests

We are using Continuous Integration and Continuous Delivery are the two practices used in the software development. Continuous Integration is where the automated tests occur. The purpose of practicing CI is to ensure that the software is not broken when new commits are integrated into main branch. 
Therefore, scripts for automated tests and a CI/CD pipeline is set up on GitLab. The files committed are being tested in CI/CD pipelines. The tests will provide the status of the test and time taken for the test to complete. Commit that pass the tests will be merged into main branch.
CI practice can reduce the number of bug issues upon software deployment. Besides, cost needed for testing can be reduced as automated tests could handle a large number of tests in a short amount of time.

## Deployment

The software can be run in any local machine that has the components required to run the codes. The instructions to run the models are provided in Instructions.md .


## Built With

* [python3](https://www.python.org/) - The main programming language used
* [numpy](https://numpy.org/) - A Python package for data preprocessing
* [pandas](https://pandas.pydata.org/) - A Python library for data analysis
* [scikit-learn](https://scikit-learn.org/stable/) - Machine learning tool in Python
* [TensorFlow](https://www.tensorflow.org/) - Tool used for development of neural network
* [JupyterHub](https://jupyter.org/hub) - Collaborative development platform      
* [GDAL](https://gdal.org/) - Translator library for vector geospatial and raster data formats file


## Authors
* **Magnus Oliver Boddington Bradley** 
* **Luhan Cheng**
* **Muhammad Shahmil Shahidan**


## Acknowledgments
We would like to acknowledge these parties
* [TensorFlow](https://www.tensorflow.org/) for providing tutorials on neural networks.
* [pandas](https://pandas.pydata.org/) as the fundamental of data processing.
* [scikit-learn](https://scikit-learn.org/stable/) as the fundamental tool for machine learning.
* [JupyterHub](https://jupyter.org/hub) as our collaborative environment for this project.


