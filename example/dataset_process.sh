#!/bin/bash
# download dataset from google drive, it will generate five zipped files
# 

# recursively unzip
# may need to run a few times, because there are nested zip files
find . -name "*.zip" | while read filename; do unzip -o -d "`dirname "$filename"`" "$filename";rm "$filename"; done;

# don't know where this from
rm -rf __MACOSX/
rm -rf DELWP\ Project/Complementary\ map\ data/__MACOSX/

# this line need to be modified
export DATASET_DIR=/mnt/dataset

export RASTER_DIR=$DATASET_DIR/raster
export SHAPE_DIR=$DATASET_DIR/shape
export OBS_DIR=$DATASET_DIR/obs
export METADATA_DIR=$DATASET_DIR/metadata

find . -name "*.hdr" | while read filename; do base="$(basename "$filename")";base="$(echo "$base" | cut -d '.' -f 1)"; mkdir -p $DATASET_DIR/raster/$base; mv "$(dirname "$filename")/""${base}"* $DATASET_DIR/raster/$base;done

find . -name "*.shp" | while read filename; do base="$(basename "$filename")"; base="$(echo "$base" | cut -d '.' -f 1)"; mkdir -p $DATASET_DIR/shape/$base; mv "$(dirname "$filename")/""${base}"* $DATASET_DIR/shape/$base;done

mkdir -p $OBS_DIR
mv DELWP\ Project/Species\ observations/* $OBS_DIR/
rmdir DELWP\ Project/Species\ observations/
rmdir DELWP\ Project/Raster\ data/
mkdir -p $METADATA_DIR
mv DELWP\ Project/* $METADATA_DIR/
rmdir DELWP\ Project

# change to dataset directory disable group write
chown root:jupyterhub-user -R dataset/
chmod g-w -R dataset
