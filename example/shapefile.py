#!/usr/bin/env python
# coding: utf-8

# In[12]:


import geopandas as gpd 
import os 
import pandas as pd 
import functools
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from constants import SHAPEFILE_DIR

def inspect_column(col, dataset_dictionary):
    for i, (key, item) in enumerate(dataset_dictionary.items()):
        if col in item.columns:
            print("filename : {} \nvalues : {} \ncolumn : {}\n".format(
                key, item[col].unique(), col))

def flatten_list(l):
    flattened = []
    for p in l:
        for k in p:
            flattened.append(k)
    return flattened



class shape_dataset:
    def __init__(self, parent_paths):
        self.dataset_dict = {}
        for p in parent_paths:
            gpdf = gpd.read_file(p / (p.name + '.shx'))
            self.dataset_dict.update({p.name : gpdf})
#             column_mapping = open(p / (p.name + '.txt'),'r')
        return 


all_files = list(SHAPEFILE_DIR.glob('*'))

shapefile_dataset = shape_dataset(all_files)

# accepted_extensions = ['.shx', '.shp', '.prj', '.cpg', '.dbf', '.txt']

# dataset_dict : {filename : geopandas.geodataframe.GeoDataFrame}
# dataset_dict = {}
# for i in all_files:
#     if i.endswith('.shx'):
#         gpdf = gpd.read_file(os.path.join(REFERENCE_DATA_PATH, i))
#         dataset_dict.update({os.path.splitext(i)[0]: gpdf})

# all_columns = list(set(flatten_list([list(d.columns) for (_, d) in dataset_dict.items()])))

# inspect_column(all_columns[0], dataset_dict)


# In[13]:


shapefile_dataset.dataset_dict['VMLITE_FOREST_SU5']


# In[3]:





# In[ ]:




