from pathlib import Path

# This is the root of software, it is the parent directory of this file (constants.py)
ROOT_DIR = Path(__file__).resolve().parent
SUBMISSIONS = ROOT_DIR / 'submissions'

# path definitin of raw datasets
DATASET_DIR = ROOT_DIR / 'dataset'
RASTER_DIR = DATASET_DIR / 'raster'
SHAPEFILE_DIR = DATASET_DIR / 'shape'

## When initiliasing data and building models
OBS_CSV = DATASET_DIR / 'obs' / 'Monash_sample_VBA.xls'
# When accepting submissions
# OBS_CSV = SUBMISSIONS / 'submission.xls'


# this is the path to where the processed dataset will be saved
## When initiliasing data and building models
PROCESSED_DATASET = DATASET_DIR / 'processed'
## When accepting submissions
# PROCESSED_DATASET = DATASET_DIR / 'testing' / 'output'

PROCESSED_RASTER_DATASET = PROCESSED_DATASET / 'raster'
PROCESSED_SHAPE_DATASET = PROCESSED_DATASET / 'shape'
ABSENCE_DATASET = PROCESSED_DATASET / 'absence'

# this is the path where the models will saved
MODELS = ROOT_DIR / 'models'

# this is the path where new observations will be submitted


# the columns which will be used in our modelling
OBS_COL = ["SCIENTIFIC_DISPLAY_NME","COMMON_NME","TAXON_TYPE","REVIEW_COMMENT_TXT", "BEHAVIOUR","EXTRA_INFORMATION","PRIMARY_CDE","STATE_CDE","LGA_UFI","CMA_NO","SITE_ORIGIN_STATUS_CDE"]
RASTER_COL = [i.stem for i in RASTER_DIR.glob('*')]
SHAPEFILE_COL = ['VMLITE_BUILT_UP_AREA','VMLITE_FOREST_SU2','VMLITE_PUBLIC_LAND_SU5','VMLITE_HY_WATER_AREA','VMLITE_TR_ROAD']
NORMALIZED_DATASET = PROCESSED_DATASET / 'normalized_dataset.xls'

TAXON_IDS = [11028, 13182, 504391, 60555, 10561, 503998]


